#include<stdio.h>
#include<conio.h>
int main()
{
	int array[] = {1, 3, 4, 2, 5, 6, 9, 8};
 	int len = sizeof(array) / sizeof(array[0]);
 	int result = check(array ,len);
 	printf("Missing number = %d", result);
}
int check(int array[], int len)
{
	int i, j, n, sum_total, sum_array = 0;
	for(i = 0; i <len ;i++)
	{
		for( j = i+1; j < len; j++)
		{
			if(array[i] > array[j])
			{
				int temp = array[j];
				array[j] = array[i];
				array[i] = temp;
			}
		}
	}// Sorting the given array in ascending order
	  n = array[len - 1];
	 sum_total = n * (n  + 1) / 2 ;//sum of numbers from 1 to last index
	  for(i = 0; i < len; i++)
	  {
	  	sum_array += array[i]; // sum of all array element
	  }
	 return (sum_total - sum_array);
}
